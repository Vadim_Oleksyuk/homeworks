package collections.taskmanager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TaskManagerImpl implements TaskManager {
    private Map<Date, Task> tasks = new HashMap<Date, Task>();

    public TaskManagerImpl (){}


    public static void main(String[] args) {
        TaskManager taskManager = new TaskManagerImpl();

        Task task1 = new Task("height priority", "develop task manager");
        Calendar calendar = new GregorianCalendar(2015,1,17,13,0);
        taskManager.addTask(calendar.getTime(),task1);

        Task task2 = new Task("low priority","analis spring");
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        taskManager.addTask(calendar.getTime(),task2);

        Task task3 = new Task("height priority", "go to church");
        calendar.set(Calendar.HOUR_OF_DAY, 17);
        taskManager.addTask(calendar.getTime(), task3);

        Task task4 = new Task("height priority", "go to university");
        calendar.add(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 15);
        taskManager.addTask(calendar.getTime(), task4);

        Task task5 = new Task("low priority","buy bread");
        calendar.set(Calendar.HOUR_OF_DAY, 9);
        taskManager.addTask(calendar.getTime(), task5);


        Task task6 = new Task("height priority", "go to church");
        calendar.set(Calendar.HOUR_OF_DAY, 17);
        taskManager.addTask(calendar.getTime(), task6);
        System.out.println(taskManager.getCategories());

        Map<String,List<Task>> taskListByCategories = taskManager.getTaskByCategories();
        System.out.println(taskListByCategories.toString());

        List<Task> tasksForCategory = taskManager.getTaskByCategory("height priority");
        System.out.println(tasksForCategory.toString());

        List<Task> tasksForToday = taskManager.getTaskForToday();
        System.out.println(tasksForToday.toString());

    }

    @Override
    public void addTask(Date date, Task task) {
        tasks.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        tasks.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        Set<String> categories = new HashSet<String>();

        for (Task task: tasks.values()) {
            categories.add(task.getCategory());
        }

        return categories;
    }

    @Override
    public Map<String, List<Task>> getTaskByCategories() {
        Map<String, List<Task>> mapTasksByCategories = new HashMap<String, List<Task>>();

        for (String category: getCategories()){
            List<Task> tasksListForCategory = new ArrayList<Task>();

            for (Date date: getSortedDateKeys()){
                Task task = tasks.get(date);
                if (task.getCategory().equals(category)){
                    tasksListForCategory.add(task);
                }
            }
            mapTasksByCategories.put(category, tasksListForCategory);
        }

        return mapTasksByCategories;
    }

    @Override
    public List<Task> getTaskByCategory(String category) {
        return getTaskByCategories().get(category);
    }

    @Override
    public List<Task> getTaskForToday() {
        List<Task> tasksForToday = new ArrayList<Task>();

        for (Date date: getSortedDateKeys()){
            if (isToday(date)) {
                tasksForToday.add(this.tasks.get(date));
            }
        }
        return tasksForToday;
    }

    private boolean isToday(Date date) {
        Calendar scheduledDate = Calendar.getInstance();
        scheduledDate.setTime(date);
        Calendar today = Calendar.getInstance();

        if (today.get(Calendar.YEAR) == scheduledDate.get(Calendar.YEAR)&
            today.get(Calendar.MONTH)== scheduledDate.get(Calendar.MONTH)&
            today.get(Calendar.DATE) == scheduledDate.get(Calendar.DATE)) {
            return true;
        }

        return false;
    }

    public Collection<Date> getSortedDateKeys() {
        List<Date> sortedKeys = new ArrayList<Date>(tasks.keySet());
        Collections.sort(sortedKeys, new Comparator<Date>() {
            public int compare(Date date1, Date date2) {
                return date1.compareTo(date2);
            }
        });
        return sortedKeys;
    }
}
