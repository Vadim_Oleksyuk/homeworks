package collections.taskmanager;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TaskManager {
    public void addTask(Date date, Task task);
    public void removeTask(Date date);
    public Collection<String> getCategories();

    //for next 3 methods tasks should be sorted by scheduled date
    public Map<String, List<Task>> getTaskByCategories();
    public List<Task> getTaskByCategory(String category);
    public List<Task> getTaskForToday();
}
