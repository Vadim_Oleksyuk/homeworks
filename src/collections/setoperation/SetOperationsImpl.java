package collections.setoperation;


import java.util.HashSet;
import java.util.Set;

public class SetOperationsImpl implements SetOperations {

    public static void main(String[] args) {
        Set a = new HashSet();
        a.add(5);
        a.add("orange");
        a.add("banana");
        a.add(new Integer(7));
        Set b = new HashSet();
        b.add(5);
        b.add("orange");
        b.add("tomato");
        b.add(8);

        System.out.println("set A="+a);
        System.out.println("set B="+b);
        System.out.println("==============");
        SetOperations setOperations = new SetOperationsImpl();
        System.out.println("a union b:");
        System.out.println(setOperations.union(a, b));
        System.out.println("--------------");
        System.out.println("a subtract b:");
        System.out.println(setOperations.subtract(a, b));
        System.out.println("--------------");
        System.out.println("a intersect b:");
        System.out.println(setOperations.intersect(a, b));
        System.out.println("--------------");
        System.out.println("a symmetric subtract b:");
        System.out.println(setOperations.symmetricSubtract(a, b));
        System.out.println("--------------");

    }

    @Override
    public boolean equals(Set a, Set b) {
        return a.equals(b);
    }

    @Override
    public Set union(Set a, Set b) {
        Set unionSet = new HashSet();
        unionSet.addAll(a);
        unionSet.addAll(b);

        return unionSet;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set resultSet = new HashSet();
        for (Object o : a) {
            if (!b.contains(o))
                resultSet.add(o);
        }

        return resultSet;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set resultSet = new HashSet();

        for (Object o : a) {
            if (b.contains(o))
                resultSet.add(o);
        }

        return resultSet;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        return union(subtract(a, b), subtract(b, a));
    }
}
